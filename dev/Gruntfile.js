/* eslint no-undef: 0 */
module.exports = function(grunt) {
  'use strict';
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({

    config: {
      source: '.',
      dest: '../',
      temp: '.tmp'
    },

    eslint: {
      // http://eslint.org/docs/rules/
      target: '<%= config.source %>/js/**/*'
    },

    sasslint: {
      // https://github.com/sasstools/sass-lint/tree/master/docs/rules
      target: '<%= config.source %>/scss/**/*'
    },

    sass: {
      options: {
        style: 'compressed',
        sourceMap: true,
        includePaths: [
          '<%= config.source %>/scss/',
          './node_modules/'
        ]
      },
      build: {
        files: {
          '<%= config.temp %>/style.css': '<%= config.source %>/scss/main.scss',
        }
      }
    },

    postcss: {
      options: {
        map: {
          annotation: true,
          sourcesContent: true
        },
        processors: [
          // browser list is managed in package.json
          // http://browserl.ist/?q=defaults
          require('autoprefixer'),
          require('postcss-csso'),
        ]
      },
      dist: {
        src: '<%= config.temp %>/style.css'
      }
    },

    concat: {
      options: {
        stripBanners: false,
        sourceMap: true,
      },

      js: {
        options: { separator: ';\n', },
        src: [
          "node_modules/jquery/dist/jquery.min.js",
          "node_modules/smoothscroll-polyfill/dist/smoothscroll.min.js",
          "<%= config.temp %>/main.min.js"
        ],
        dest: "<%= config.dest %>/main.min.js",
      },

      css: {
        src: [
          "<%= config.source %>/theme.css",
          "node_modules/normalize.css/normalize.css",
          "<%= config.temp %>/style.css",
        ],
        dest: "<%= config.dest %>/style.css",
      }
    },

    uglify: {
      options: {
        sourceMap: true,
      },
      main: {
        files: [{
          src: [
            "<%= config.source %>/js/main/*.js",
            "<%= config.source %>/js/main.js"
          ],
          dest: "<%= config.temp %>/main.min.js"
        }]
      },
    },

    watch: {
      scss: {
        files: [
          '<%= config.source %>/**/*.scss',
          '<%= config.source %>/theme.css',
        ],
        tasks: ['concurrent:scssWatch']
      },
      js: {
        files: '<%= config.source %>/**/*.{json,js}',
        tasks: ['concurrent:jsWatch']
      },
    },

    concurrent: {
      scssWatch: ['sasslint', 'buildScss'],
      jsWatch:   ['eslint',   'buildJs'],
    }

  });

  // Tasks
  grunt.registerTask('lint',      ['sasslint', 'eslint']);
  grunt.registerTask('buildJs',   ['uglify', 'concat:js']);
  grunt.registerTask('buildScss', ['sass', 'postcss', 'concat:css']);

  grunt.registerTask('build',   ['lint', 'buildJs', 'buildScss']);
  grunt.registerTask('serve',   ['lint', 'buildJs', 'buildScss', 'watch']);
  grunt.registerTask('default', 'build');
};
