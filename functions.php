<?php

function cjdoula_script_enqueue() {

  wp_enqueue_style ('cjdoulastyle', get_template_directory_uri() . '/dev/scss/main.css', array(), '1.0.0', 'all');
  wp_enqueue_script ('cjdoulajs', get_template_directory_uri() . '/dev/js/main.js', array (), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'cjdoula_script_enqueue');

function cjdoula_theme_setup() {

  add_theme_support('menus');

  register_nav_menu('primary', 'Primary Header Navigation');
  register_nav_menu('secondary', 'Footer Navigation');
}

add_action('init', 'cjdoula_theme_setup');

add_theme_support('custom-background');
add_theme_support('custom-header');
add_theme_support('post-thumbnails');
?>
