<?php

/*
  Template Name: Title with Circle Image
*/

get_header(); ?>

  <div class="content-container">
    <?php
    if( have_posts() ):
      while( have_posts() ): the_post(); ?>
        <div class="circle-img">
          <?php the_post_thumbnail('thumbnail'); ?>
        </div>
        <h2><?php the_title();?></h2>
        <p><?php the_content();?></p>
      <?php endwhile;
    endif;
    ?>
  </div>

<?php get_footer(); ?>
