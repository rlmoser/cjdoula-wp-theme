<?php

/*
  Template Name: Title with Medium Image
*/

get_header(); ?>

  <div class="content-container">
    <?php
    if( have_posts() ):
      while( have_posts() ): the_post(); ?>
        <div class="medium-img">
          <?php the_post_thumbnail('medium'); ?>
          <h2 class="tag-line">Helping create a more positive birth experience</h2>
        </div>
        <h2><?php the_title();?></h2>
        <p><?php the_content();?></p>
      <?php endwhile;
    endif;
    ?>
  </div>

<?php get_footer(); ?>
