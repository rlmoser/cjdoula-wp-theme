<?php

/*
  Template Name: Page No Title
*/

get_header(); ?>

  <div class="content-container">
    <?php
    if( have_posts() ):
      while( have_posts() ): the_post(); ?>
        <div class="page-img">
          <?php the_post_thumbnail('large'); ?>
          <h2 class="tag-line">Helping create a more positive birth experience</h2>
        </div>
        <p><?php the_content();?></p>
      <?php endwhile;
    endif;
    ?>
  </div>

<?php get_footer(); ?>
