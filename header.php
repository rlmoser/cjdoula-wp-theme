<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
      cjdoula Theme
    </title>
    <?php wp_head(); ?>
  </head>

<?php
  if (is_front_page() ):
    $cjdoula_class = array ( 'cjdoula-home-class');
  else:
    $cjdoula_class = array ( 'not-home-class');
  endif;
?>

  <body <?php body_class( $cjdoula_class ); ?>>
    <header>
        <div class="header-container">
          <img class="cjdoula-logo" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" />
          <div class="header-title">
            <h1>Charity Jeffs</h1>
            <p>DONA trained birth doula</p>
          </div>
        </div>
      <?php wp_nav_menu(array('theme_location'=>'primary')); ?>
    </header>
