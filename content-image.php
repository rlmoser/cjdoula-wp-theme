<h1><?php the_title();?></h1>
<div class="medium-img"><?php the_post_thumbnail('medium'); ?></div>
<p><?php the_content();?></p>
